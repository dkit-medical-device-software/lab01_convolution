import unittest
import numpy as np

from convolution_exercise import convolve

class TestConvolutionFunction(unittest.TestCase):

    def setUp(self):
        self.x = np.array([1, 7, -3, 5, -7])
        
    def test_identity(self):
        h = np.array([1])
        y = convolve(self.x, h)
        self.assertTrue(np.array_equal(y, self.x))

    def test_null(self):
        h = np.array([0])
        y = convolve(self.x, h)
        self.assertTrue(np.array_equal(y, np.array([0, 0, 0, 0, 0])))
        
    def test_2pt_moving_sum(self):
        h = np.array([1, 1])
        y_expected = np.array([1, 8, 4, 2, -2, -7])
        y = convolve(self.x, h)
        self.assertTrue(np.array_equal(y, y_expected))

    def test_commutativity(self):
        h = np.array([1, 1])
        y_expected = np.array([1, 8, 4, 2, -2, -7])
        y = convolve(h, self.x)
        self.assertTrue(np.array_equal(y, y_expected))        
        
