import unittest
import numpy as np

from convolution_machine_exercise import ConvolutionMachine

class TestConvolutionFunction(unittest.TestCase):

    def setUp(self):
        self.x = np.array([1, 7, -3, 5, -7])
        
    def test_identity(self):
        machine = ConvolutionMachine(np.array([1]))
        for x_val in self.x:
            self.assertEqual(x_val, machine.step(x_val))

    def test_null(self):
        machine = ConvolutionMachine(np.array([1]))
        for x_val in self.x:
            self.assertEqual(0, machine.step(x_val))
        
    def test_2pt_moving_sum(self):
        machine = ConvolutionMachine(np.array([1, 1]))
        # never going to execute the last step so length same as x.
        y_expected = np.array([1, 8, 4, 2, -2])
        for (x_val, y_expected_val) in zip(self.x, y_expected):
            self.assertEqual(y_expected_val, machine.step(x_val))

    def test_commutativity(self):
        machine = ConvolutionMachine(self.x)
        h = np.array([1, 1])
        y_expected = np.array([1, 8])
        for (x_val, y_expected_val) in zip(self.x, y_expected):
            self.assertEqual(y_expected_val, machine.step(x_val))
        
