===============
Convolution lab
===============

Aims
----

#. Introduce you to lab-level unit testing.
#. Implement your own convolution function from first principles.
#. Consider a convolution "machine" rather than function.
#. Adapt convolution to perform the correlation operation.


Cloning the source code
-----------------------

Using a git client (of any sort) clone the source code for this lab to your local machine.
   
   
Exercise 1
----------

In ``convolution_exercise.py``, implement a function that convolves two signals according to the standard rules of discrete convolution.

Try to include a demo including plotting to demonstrate that your function works.

Also, make sure that your function passes the unit tests in ``convolution_test.py``.


Exercise 2
----------

In ``convolution_machine_exercise.py``, implement a class that is able to perform a convolution at each received input sample point returning the y[n] for the given x[n].

Try to include a demo including plotting (or a console listing) to demonstrate that your function works.

Also, make sure that your function passes the unit tests in ``convolution_machine_test.py``.


Exercise 3
----------

Build a correlation function and machine, constructing unit tests for each.

