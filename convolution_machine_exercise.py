"""
Exercise 2: building a convolution machine

Most embedded devices don't have the luxury of working on nice arrays of known length, in contiguous memory etc.
Instead, they operate on inputs as they're received.

The convolution machine should take in a single input value and return a single output value each time that step is called.

"""

class ConvolutionMachine:
    def __init__(self, h):
        """ constructor, if required """
        pass

    def step(self, x):
        return 0

"""

try to include a demo (perhaps printing to the console) of a sample input and output, following example1.

to instantiate a class just use ConvolutionMachine() and similar.

"""
