"""
Exercise 1 : Build a convolution function

You are asked to implement convolution in the convolution(x,h) function below.

As this is an implementation exercise, you cannot use np.convolve (or any other similar library function).

Unit tests are provided in convolution_test.py and can be run as follows:

   python3 -m nose convolution_test

"""

import numpy as np

def convolve(x, h):
    """
    Replace this blank stub with your convolution function.
    """
    pass

if __name__ == '__main__':
    """
    You can replace pass with your own demo code here..

    Try to write some demonstration code that exercises your convolution function and plots the result in a graph.

    This will be executed when this file is run from the command line (or your IDE), e.g.
    
       python3 -m convolution_exercise 
       python3 convolution_exercise.py

    """
    pass
